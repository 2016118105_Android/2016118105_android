package com.example.somuns.chap07_providertest;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private String newId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button add=(Button) findViewById(R.id.add);
        Button select=(Button) findViewById(R.id.select);
        add.setOnClickListener(this);
        select.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Uri uri;
        switch (v.getId()){
            case R.id.add:
                uri= Uri.parse("content://com.example.somuns.chap07/book");
                ContentValues values=new ContentValues();
                values.put("name","怎么赚钱");
                values.put("author","大米");
                values.put("pages","1040");
                values.put("price","1000000");
                Uri newUri=getContentResolver().insert(uri,values);
                newId=newUri.getPathSegments().get(1);
                break;
            case R.id.select:
                uri= Uri.parse("content://com.example.somuns.chap07/book");
                Cursor cursor=getContentResolver().query(uri,null,null,null,null);
                if(cursor!=null){
                    while(cursor.moveToNext()){
                        String name=cursor.getString(cursor.getColumnIndex("name"));
                        String author=cursor.getString(cursor.getColumnIndex("author"));
                        int pages=cursor.getInt(cursor.getColumnIndex("price"));
                        double price=cursor.getDouble(cursor.getColumnIndex("price"));
                        Log.d("MainActivity","book name is"+name);
                        Log.d("MainActivity","book author "+author);
                        Log.d("MainActivity","book pages "+pages);
                        Log.d("MainActivity","book prices "+price);
                    }
                    cursor.close();
                    break;
                }
        }
    }
}
