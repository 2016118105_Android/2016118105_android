package com.example.a77101.chap02_01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.a77101.chap02_01.Collector.ActivityCollector;

public class HelloWorld extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_world);
        setAllListeners();
        ActivityCollector.addActivity(this);
        Log.d("BaseActivity",getClass().getSimpleName());
    }

    public void setAllListeners(){
        Button hey=(Button)this.findViewById(R.id.Hey);
        Button hi=(Button)this.findViewById(R.id.HI);
        Button hello=(Button)this.findViewById(R.id.hello);
        Button standard=(Button)this.findViewById(R.id.Standard);
        Button singleTop=(Button)this.findViewById(R.id.singleTop);
        Button singleTask=(Button)this.findViewById(R.id.singleTask);
        Button singleInstance=(Button)this.findViewById(R.id.singleInstance);
        Button exit=(Button)findViewById(R.id.exit);
        exit.setOnClickListener(this);
        hey.setOnClickListener(this);
        hi.setOnClickListener(this);
        hello.setOnClickListener(this);
        standard.setOnClickListener(this);
        singleTop.setOnClickListener(this);
        singleInstance.setOnClickListener(this);
        singleTask.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollector.removeActivity(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch(v.getId()){
            case R.id.Hey:
                intent = new Intent(HelloWorld.this,HeyActivity.class);
                startActivity(intent);break;
            case R.id.HI:
                intent = new Intent(HelloWorld.this,HiActivity.class);
                startActivity(intent);break;
            case R.id.hello:
                intent = new Intent(HelloWorld.this,HelloWorld.class);
                startActivity(intent);break;
            case R.id.singleInstance:
                intent = new Intent(HelloWorld.this,singleInstance.class);
                startActivity(intent);break;
            case R.id.singleTask:
                intent = new Intent(HelloWorld.this,singleTask.class);
                startActivity(intent);break;
            case R.id.singleTop:
                intent = new Intent(HelloWorld.this,singleTop.class);
                startActivity(intent);break;
            case R.id.Standard:
                intent = new Intent(HelloWorld.this,standard.class);
                startActivity(intent);break;
            case R.id.exit:
                ActivityCollector.finishAll();break;

        }
    }
}
