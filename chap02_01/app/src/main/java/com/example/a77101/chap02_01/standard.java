package com.example.a77101.chap02_01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class standard extends HelloWorld {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard);
        setTitle("standard");
        Button standard=(Button)this.findViewById(R.id.Standard);
        Button singleTop=(Button)this.findViewById(R.id.singleTop);
        Button singleTask=(Button)this.findViewById(R.id.singleTask);
        Button singleInstance=(Button)this.findViewById(R.id.singleInstance);
        Button exit=(Button)findViewById(R.id.exit);
        exit.setOnClickListener(this);
        standard.setOnClickListener(this);
        singleTop.setOnClickListener(this);
        singleInstance.setOnClickListener(this);
        singleTask.setOnClickListener(this);

    }
}
