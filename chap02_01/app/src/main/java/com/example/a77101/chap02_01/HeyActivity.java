package com.example.a77101.chap02_01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HeyActivity extends HelloWorld implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hey);
        Button hi=(Button)this.findViewById(R.id.HI);
        Button Hello=(Button)this.findViewById(R.id.Hello);
        Button standard=(Button)this.findViewById(R.id.Standard);
        Button singleTop=(Button)this.findViewById(R.id.singleTop);
        Button singleTask=(Button)this.findViewById(R.id.singleTask);
        Button singleInstance=(Button)this.findViewById(R.id.singleInstance);
        Button exit=(Button)findViewById(R.id.exit);
        exit.setOnClickListener(this);
        hi.setOnClickListener(this);
        Hello.setOnClickListener(this);
        standard.setOnClickListener(this);
        singleTop.setOnClickListener(this);
        singleInstance.setOnClickListener(this);
        singleTask.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Intent intent;

        switch(v.getId()){
            case R.id.Hello:
                intent = new Intent("com.example.a77101.chap02_01.hello_action_start");
                startActivity(intent);break;
            case R.id.HI:
                intent = new Intent(HeyActivity.this,HiActivity.class);
                startActivity(intent);break;
        }

    }

}
