package com.example.a77101.chap02_01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HiActivity extends HelloWorld implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hi);
        Button hey=(Button)this.findViewById(R.id.Hey);
        Button hello=(Button)this.findViewById(R.id.Hello);
        hey.setOnClickListener(this);hello.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch(v.getId()){
            case R.id.Hello:
                intent = new Intent("com.example.a77101.chap02_01.hello_action_start");
                startActivity(intent);break;
            case R.id.Hey:
                intent = new Intent(HiActivity.this,HeyActivity.class);
                startActivity(intent);break;
        }
    }
}
