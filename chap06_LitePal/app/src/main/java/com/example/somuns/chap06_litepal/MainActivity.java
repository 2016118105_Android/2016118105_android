package com.example.somuns.chap06_litepal;

import android.graphics.ColorSpace;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.litepal.crud.DataSupport;
import org.litepal.tablemanager.Connector;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button createDB=(Button)findViewById(R.id.createDB);
        createDB.setOnClickListener(this);
        Button insert=(Button)findViewById(R.id.insert);
        insert.setOnClickListener(this);
        Button select=(Button)findViewById(R.id.select);
        select.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.createDB:
                Connector.getDatabase();break;
            case R.id.insert:
                insertData();
                break;
            case R.id.select:
                List<Book> books=selectAllBook();
                for(Book book:books){
                    Log.d("Main","book name is"+book.getName());
                    Log.d("Main","book id is"+book.getId());
                    Log.d("Main","book price is"+book.getPrice());
                }
                break;
        }
    }

    private List<Book> selectAllBook(){
        List<Book> list= DataSupport.findAll(Book.class);
        return list;
    }

    private void insertData(){
        Book book=new Book();
        book.setName("Android系统源代码情景分析");
        book.setPrice(101);
        book.save();
    }
}
