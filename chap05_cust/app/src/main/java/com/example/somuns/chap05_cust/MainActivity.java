package com.example.somuns.chap05_cust;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    IntentFilter intentFilter;
    private MyBroadcastReceiver myBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myBroadcastReceiver=new MyBroadcastReceiver();
        intentFilter=new IntentFilter();
        intentFilter.addAction("com.example.broadcasttest.MY_BROADCAST");
        Button b=(Button)findViewById(R.id.b);
        b.setOnClickListener(this);
        registerReceiver(myBroadcastReceiver,intentFilter);
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent("com.example.broadcasttest.MY_BROADCAST");
        sendBroadcast(intent);
    }
}
