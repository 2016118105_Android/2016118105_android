package com.example.somuns.chap09_httpurlconnect_bestpractise.Interface;

public interface HttpCallBackListener {
    void onFinish(String response);
    void onError(Exception e);
}
