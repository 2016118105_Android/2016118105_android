package com.example.somuns.chap09_httpurlconnect_bestpractise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.somuns.chap09_httpurlconnect_bestpractise.Interface.HttpCallBackListener;
import com.example.somuns.chap09_httpurlconnect_bestpractise.R;
import com.example.somuns.chap09_httpurlconnect_bestpractise.Util.HttpUtil;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView responseText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button sendRequest=(Button) findViewById(R.id.send);
        Button sendOkHttp=(Button) findViewById(R.id.send_OkHttp);
        responseText=(TextView) findViewById(R.id.response_text);
        sendRequest.setOnClickListener(this);
        sendOkHttp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.send){
            sendRequestWithHttpURLConnection();
        }
        if(v.getId()== R.id.send_OkHttp){
            sendRequestWithOkHttp();
        }
    }

    private void sendRequestWithOkHttp(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try{
                    OkHttpClient client=new OkHttpClient();
                    Request request=new Request.Builder().url("http://www.baidu.com").build();
                    Response response=client.newCall(request).execute();
                    String responseData=response.body().string();
                    showResponse(responseData);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void sendRequestWithHttpURLConnection(){
        String address="https://www.baidu.com";
        String address2="https://www.sogou.com/";
        HttpUtil.sendRequestWithHttpURLConnection(address,new HttpCallBackListener(){
            @Override
            public void onFinish(String response) {
                showResponse(response.toString());
            }

            @Override
                public void onError(Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void showResponse(final String response){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                responseText.setText(response);
            }
        });
    }
}
