package com.example.somuns.chap07;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.net.*;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayAdapter<String> adapter;
    List<String> contactsList=new ArrayList<>();
    private MyDatabaseHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper=new MyDatabaseHelper(this,"BookStore.db",null,2);
        Button create_db=(Button) findViewById(R.id.create_db);
        Button makeCall = (Button) findViewById(R.id.make_call);
        create_db.setOnClickListener(this);
        makeCall.setOnClickListener(this);
        ListView contactsView=(ListView)findViewById(R.id.contacts_view);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,contactsList);
        contactsView.setAdapter(adapter);
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.READ_CONTACTS},2);
        }else{
            readContacts();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.make_call:
            if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE)!=PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CALL_PHONE},1);
            }else{
                call();
            }break;
            case R.id.create_db:
                dbHelper.getWritableDatabase();
                break;
        }
    }

    private void call() {
        try {

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:10086"));
            startActivity(intent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
    private void readContacts(){
        Cursor cursor=null;
        try{
            //查询联系人数据
            cursor=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
            if(cursor!=null){
                while(cursor.moveToNext()){
                    String displayName=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String number=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    contactsList.add(displayName+"\n"+"number");
                }
                adapter.notifyDataSetChanged();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(cursor!=null){
                cursor.close();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantRequests){
       switch (requestCode){
           case 1:
               if(grantRequests.length>0&&grantRequests[0]== PackageManager.PERMISSION_GRANTED){
                   call();
               }else{
                   Toast.makeText(this,"您禁止了权限",Toast.LENGTH_SHORT).show();
               }break;
           case 2:
               if(grantRequests.length>0&&grantRequests[0]==PackageManager.PERMISSION_GRANTED){
                   readContacts();
               }else{
                   Toast.makeText(this,"您禁止了权限",Toast.LENGTH_SHORT).show();
               }break;
               default:
       }
    }
}
