package com.example.somuns.chap05_best_practice.com.sen.Activitys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.somuns.chap05_best_practice.ActivityCollector;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener{
    private ForceOfflineReceiver FOR;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCollector.activitylist.add(this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollector.activitylist.remove(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("com.example.broadcastbestpractice.FORCE_OFFLINE");
        FOR=new ForceOfflineReceiver();
        registerReceiver(FOR,intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(FOR!=null){
            unregisterReceiver(FOR);
            FOR=null;
        }
    }

    @Override
    public void onClick(View v) {

    }
    class ForceOfflineReceiver extends BroadcastReceiver implements View.OnClickListener{
        @Override
        public void onReceive(final Context context, Intent intent) {
            AlertDialog.Builder builder=new AlertDialog.Builder(context);
            builder.setTitle("Waring");
            builder.setMessage("你已被强制下线，请重新登陆");
            builder.setCancelable(false);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCollector.finishAll();
                    Intent intent=new Intent(context,LoginActivity.class);
                    context.startActivity(intent);
                }
            });
            builder.show();
        }

        @Override
        public void onClick(View v) {

        }
    }
}
