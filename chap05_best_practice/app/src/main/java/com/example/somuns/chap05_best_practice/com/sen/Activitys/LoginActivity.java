package com.example.somuns.chap05_best_practice.com.sen.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

import com.example.somuns.chap05_best_practice.R;
import com.example.somuns.chap05_best_practice.com.sen.Activitys.BaseActivity;

public class LoginActivity extends BaseActivity {
    private EditText accountEdit;
    private EditText passwordEdit;
    private Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        accountEdit=(EditText)findViewById(R.id.account);
        passwordEdit=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.login);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String account=accountEdit.getText().toString();
        String pwd=passwordEdit.getText().toString();
        if(account.equals("admin")&&pwd.equals("123")){
            Intent intent=new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        else{
            Toast.makeText(LoginActivity.this,"account or password error!",Toast.LENGTH_SHORT).show();
        }
    }
}
