package com.example.somuns.chap05_best_practice.com.sen.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.somuns.chap05_best_practice.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button f=(Button) findViewById(R.id.force_offline);f.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent("com.example.broadcastbestpractice.FORCE_OFFLINE");
        sendBroadcast(intent);
    }
}
