package com.example.somuns.chap05_best_practice;
import java.util.*;
import android.app.Activity;

public class ActivityCollector {
    public static List<Activity> activitylist=new ArrayList<>();
    public static void addActivity(Activity activity){
        activitylist.add(activity);
    }
    public static void removeActivity(Activity activity){
        activitylist.remove(activity);
    }
    public static void finishAll(){
        for(Activity activity:activitylist){
            if(!activity.isFinishing()){
                activity.finish();

            }
        }
    }
}
